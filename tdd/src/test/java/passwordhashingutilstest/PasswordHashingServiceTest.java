package passwordhashingutilstest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import passwordhashingutils.PasswordHashingService;


import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class PasswordHashingServiceTest {

    private PasswordHashingService passwordHashingService;
    private String hashedPassword;

    @BeforeEach
    public void setUp() throws InvalidKeySpecException, NoSuchAlgorithmException {
        passwordHashingService = new PasswordHashingService();
        hashedPassword = passwordHashingService.hashPass("123");
    }

    @Test
    public void hash_password_test_should_hash_password(){
        assertThat(hashedPassword, notNullValue());
        assertThat(hashedPassword.length(), equalTo(88));
    }
    @Test
    public void verifying_password_test() throws InvalidKeySpecException, NoSuchAlgorithmException {
         boolean isPasswordValid = passwordHashingService.verifyPassword("123", hashedPassword);
         assertThat(isPasswordValid, equalTo(true));
    }
    @Test
    public void verifying_password_should_fail_test() throws InvalidKeySpecException, NoSuchAlgorithmException {
        boolean isPasswordValid = passwordHashingService.verifyPassword("1234", hashedPassword);
        assertThat(isPasswordValid, equalTo(false));
    }
}
