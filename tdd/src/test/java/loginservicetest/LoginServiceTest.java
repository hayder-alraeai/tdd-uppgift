package loginservicetest;
import loginservice.LoginService;
import loginservice.NotAuthorizedExeption;
import models.Authorities;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import passwordhashingutils.PasswordHashingService;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LoginServiceTest {
    private LoginService loginService;
    private PasswordHashingService passwordHashingService;

    @BeforeEach
    public void setUp() throws InvalidKeySpecException, NoSuchAlgorithmException {
        passwordHashingService = mock(PasswordHashingService.class);
        loginService = new LoginService(passwordHashingService);
    }
    @Test
    public void login_should_return_token_test() throws InvalidKeySpecException, NoSuchAlgorithmException {
        when(passwordHashingService.verifyPassword(anyString(), anyString() )).thenReturn(true);
        String token = loginService.login("anna", "losen");
        assertThat(token, notNullValue());
        assertThat(token, startsWith("ey"));
    }
    @Test
    public void login_should_throw_IllegalStateException_with_wrong_username_test() {
        assertThrows(IllegalStateException.class, () -> {
            loginService.login("jonas", "losen");//wrong username
        });
    }
    @Test
    public void login_should_throw_IllegalStateException_with_wrong_password_test() {
        assertThrows(IllegalStateException.class, () -> {
            loginService.login("anna", "solen");//wrong password
        });
    }

    @Test
    public void check_whether_token_isValid_test() throws InvalidKeySpecException, NoSuchAlgorithmException {
        when(passwordHashingService.verifyPassword(anyString(), anyString() )).thenReturn(true);
        String token = loginService.login("berit", "123456");
        boolean isTokenValid = loginService.isTokenValid(token);
        assertThat(isTokenValid, equalTo(true));
    }
    @Test
    public void check_Authorities_with_different_resources_test() throws NotAuthorizedExeption, InvalidKeySpecException, NoSuchAlgorithmException {
        when(passwordHashingService.verifyPassword(anyString(), anyString() )).thenReturn(true);
        String token = loginService.login("berit", "123456");
        List<String> authorities = loginService.getAuthorities(token, "ACCOUNT");
        assertThat(authorities.size(), equalTo(2));
        assertThat(authorities,contains("READ", "WRITE"));
    }
    @Test
    public void check_with_other_resource_test() throws NotAuthorizedExeption, InvalidKeySpecException, NoSuchAlgorithmException {
        when(passwordHashingService.verifyPassword(anyString(), anyString() )).thenReturn(true);
        String token = loginService.login("kalle", "password");
        List<String> authorities = loginService.getAuthorities(token, "PROVISION_CALC");
        assertThat(authorities.size(), equalTo(1));
        assertThat(authorities, contains(Authorities.EXECUTE.toString()));
    }
    @Test
    public void should_throw_notauthorizedexeption() throws NotAuthorizedExeption, InvalidKeySpecException, NoSuchAlgorithmException {
        when(passwordHashingService.verifyPassword(anyString(), anyString() )).thenReturn(true);
        String token = loginService.login("berit", "123456");
        assertThrows(NotAuthorizedExeption.class, () -> {
            loginService.getAuthorities(token, "PROVISION_CALC");
        });
    }

}
