package loginservice;

public class NotAuthorizedExeption extends Exception {

    public NotAuthorizedExeption(String message) {
        super(message);
    }
}
