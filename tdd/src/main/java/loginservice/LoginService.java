package loginservice;

import passwordhashingutils.PasswordHashingService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import models.AppUser;
import models.Authorities;
import models.UserDTO;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.*;

public class LoginService {

    private PasswordHashingService passwordHashingService;
    private List<AppUser> users ;

    public LoginService(PasswordHashingService passwordHashingService) throws InvalidKeySpecException, NoSuchAlgorithmException {
        this.passwordHashingService = passwordHashingService;

        users = Arrays.asList(
                new AppUser("anna", "0ZAbVYwmFwmFM4NQ81XublLc5P5wpTP3qvBuMUfHLmXhs6cQnSBaHMd/5ovGox9Dk8sl9Jdq/0LLgcIXRWaB+w==",
                        Arrays.asList(Authorities.READ.toString())),//losen
                new AppUser("berit", "i3hxA5INtCHqYYjzP5/5gEuIewtx5PeDqqa8+LucQ4xLPrwiwqaTbvcFx7v/CaFMB/QtnYPmeRQiYASyY7XY7g==",
                        Arrays.asList(Authorities.READ.toString(), Authorities.WRITE.toString())),//123456
                new AppUser("kalle", "0I61FRWWxxqIclpb8KNV7VvyKlgdBkIdqqzE/TPWOuKASHMrNyoL26xG0cW6clup6ikHtPRTw8bEjU2C91fwaA==",
                        Arrays.asList(Authorities.EXECUTE.toString())) //password
        );
    }

    public String login(String username, String password) throws InvalidKeySpecException, NoSuchAlgorithmException {
        UserDTO user = new UserDTO(username, password);
        for (AppUser u: users){
            if (u.getUsername().equals(user.getUsername())){
                if (passwordHashingService.verifyPassword(user.getPassword(), u.getPassword())){
                    return generateToken(u.getUsername(), u.getPermissions());
                }
            }
        }
        throw new IllegalStateException("USERNAME OR PASSWORD IS NOT CORRECT!");
    }


    public boolean isTokenValid(String token) {
        String username = Jwts.parser()
                .setSigningKey("slolsdkfjsl4545LJLJlkj44lkjlk44kj4lkj4")
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        for (AppUser u: users){
            if (u.getUsername().equals(username)){
                return true;
            }
        }
        return false;
    }

    public List<String> getAuthorities(String token, String resource) throws NotAuthorizedExeption {
        Jws<Claims> jws = Jwts.parser()
                .setSigningKey("slolsdkfjsl4545LJLJlkj44lkjlk44kj4lkj4")
                .parseClaimsJws(token);
        List<String> role = (List<String>) jws.getBody().get("Roles");
        if (resource.equalsIgnoreCase("ACCOUNT")){
            if (role.contains(Authorities.READ.toString()))
                return role;
            else if (role.contains(Authorities.WRITE.toString()))
                return role;
        }else if (resource.equalsIgnoreCase("PROVISION_CALC")){
                if (role.contains(Authorities.EXECUTE.toString()))
                    return role;
        }
        throw new NotAuthorizedExeption("NOT AUTHORIZED");
    }
    public String generateToken(String username, List<String> permissions){
        String token = Jwts.builder()
                .signWith(SignatureAlgorithm.HS512, "slolsdkfjsl4545LJLJlkj44lkjlk44kj4lkj4")
                .claim("Roles", permissions)
                .setExpiration(new Date(System.currentTimeMillis() * 10))
                .setSubject(username)
                .compact();
        return token;
    }
}
