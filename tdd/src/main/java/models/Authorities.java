package models;

public enum Authorities {
    WRITE, READ, EXECUTE;
}
