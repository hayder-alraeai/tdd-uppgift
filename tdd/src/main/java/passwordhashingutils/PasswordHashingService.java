package passwordhashingutils;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Optional;

public class PasswordHashingService {

    private final int ITERATIONS = 65536;
    private final int KEY_LENGTH = 512;
    private final String SECRET_KEY = "LKSDJFLSKDFJL2K345LKEJRLQKW4J2kjlk245LKJLK53lkjlkj";
    private final String ALGORITHM = "PBKDF2WithHmacSHA512";


    public String hashPass(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
        char[] chars = password.toCharArray();
        byte[] bytes = SECRET_KEY.getBytes();

        PBEKeySpec spec = new PBEKeySpec(chars, bytes, ITERATIONS, KEY_LENGTH);

        Arrays.fill(chars, Character.MIN_VALUE);

        try {
            SecretKeyFactory fac = SecretKeyFactory.getInstance(ALGORITHM);
            byte[] securePassword = fac.generateSecret(spec).getEncoded();
            return Base64.getEncoder().encodeToString(securePassword);

        } catch (NoSuchAlgorithmException ex) {
            throw new NoSuchAlgorithmException();

        }catch (InvalidKeySpecException ex){
            throw new InvalidKeySpecException();
        } finally{
            spec.clearPassword();
        }
    }

    public boolean verifyPassword(String password, String hashedPassword) throws NoSuchAlgorithmException, InvalidKeySpecException {
        String optEncrypted = hashPass(password);
        if (optEncrypted.isEmpty()) return false;
        return optEncrypted.equals(hashedPassword);
    }
}
